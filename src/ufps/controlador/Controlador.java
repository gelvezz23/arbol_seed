/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.controlador;

import ufps.util.colecciones_seed.ArbolBinario;

/**
 *
 * @author Javier
 */
public class Controlador<T> {
    
    ArbolBinario arbolBinario;
    
    public Controlador(){
        arbolBinario = new ArbolBinario();
    }
    
    //array esta en inorder
    public ArbolBinario<T> generarArbol(T In[], T Pre[]){
        return arbolBinario.crearIn_Pre(In, Pre);
    }
    
    public int[][] generarMatriz(T In[], T Pre[]){
        return arbolBinario.getMatrizCreación(In, Pre);
    }
    
    public static void main(String[] args){
        //"D","B","E","A","H","F","I","C","G"
        //"H","D","I","B","J","E","K","A","F","C","G"
        String[] in = {"H","D","I","B","J","E","K","A","F","C","G"};
        
        //"A","B","D","E","C","F","H","I","G"
        //"A","B","D","H","I","E","J","K","C","F","G"
        String[] pre = {"A","B","D","H","I","E","J","K","C","F","G"};
        
        Controlador c = new Controlador();
        c.generarArbol(in,pre);
        c.generarMatriz(in, pre);
    }
}
