/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Javier
 */
public class MiRender extends DefaultTableCellRenderer{
    
    int maxTam = 0;
    public MiRender (int max){
        maxTam = max;
    }
    public Component getTableCellRendererComponent(JTable table, 
            Object value, boolean isSelected, boolean hasFocus, int row, int column){
        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        cell.setBackground(Color.white);
        if(value instanceof Integer){
            int valor = (int)value;
            if(valor == 1){
               if(table.getValueAt(row, 0).equals(table.getValueAt(maxTam, column))){
                    cell.setBackground(Color.blue);
               }else{
                    cell.setBackground(Color.CYAN);
               }
            }else{
                cell.setBackground(Color.white);
            }
        }
        if(value instanceof String){
            cell.setFont(new Font("Arial", Font.BOLD, 12));
            cell.setBackground(Color.white);
        }
        return cell;
    }
}
